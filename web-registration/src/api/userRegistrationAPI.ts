import { UserRegistrationFormData } from "../features/userRegistration/types";
import api from "./api";

const endpoint = "/api/countries";

export const fetchUserRegistrations = async () => {
  try {
    const response = await api.get(endpoint);
    return response.data;
  } catch (error) {
    console.error("Error fetching user registrations:", error);
    return [];
  }
};

export const saveUserRegistration = async (
  userData: UserRegistrationFormData
) => {
  try {
    const response = await api.post(endpoint, userData);
    return response.data;
  } catch (error) {
    console.error("Error saving user registration:", error);
    throw error;
  }
};
