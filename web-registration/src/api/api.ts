import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:3001",
  headers: {
    // An authorization token is needed, otherwise the API will reject it
    // In a normal use case the token should be encrypted with for example a base64 format
    Authorization: `Bearer mosano2023`,
  },
});

export const loadingTracker = {
  isLoading: false,
};

// Request interceptor
api.interceptors.request.use(
  (config) => {
    // Start loading
    loadingTracker.isLoading = true;
    return config;
  },
  (error) => {
    loadingTracker.isLoading = false;
    return Promise.reject(error);
  }
);

// Response interceptor
api.interceptors.response.use(
  (response) => {
    // End loading
    loadingTracker.isLoading = false;
    return response;
  },
  (error) => {
    loadingTracker.isLoading = false;
    return Promise.reject(error);
  }
);

export default api;
