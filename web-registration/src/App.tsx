import React from "react";
import "./App.css";
import UserRegistrationForm from "./features/userRegistration/UserRegistrationForm";
import UserRegistrationInfo from "./features/userRegistration/UserRegistrationInfo";
import LanguageSwitcher from "./components/LanguageSwitcher";

const BottomRightText = () => {
  return (
    <div className="bottom-right-text">
      <p>Frederico Gomes</p>
    </div>
  );
};

const App = () => {
  return (
    <div className="app">
      <header className="app-header">
        <p>Mosano</p>
        <LanguageSwitcher />
      </header>
      <div className="main-container">
        <div className="child-container">
          <UserRegistrationForm />
        </div>
        <div className="child-container">
          <UserRegistrationInfo />
        </div>
      </div>
      <BottomRightText />
    </div>
  );
};

export default App;
