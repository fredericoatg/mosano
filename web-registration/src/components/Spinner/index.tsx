import React from "react";
import "./Spinner.css";

const LoadingSpinner = () => {
  return <div className="loading-spinner"></div>;
};

export default LoadingSpinner;
