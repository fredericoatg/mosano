import React from "react";
import { useTranslation } from "react-i18next";

const LanguageSwitcher = () => {
  const { i18n, t } = useTranslation();

  const changeLanguage = (language: string) => {
    i18n.changeLanguage(language);
  };

  return (
    <div>
      <select id="country" onChange={(e) => changeLanguage(e.target.value)}>
        <option key="0" value="en" defaultChecked>
          {t("form.en")}
        </option>
        <option key="1" value="pt">
          {t("form.pt")}
        </option>
      </select>
    </div>
  );
};

export default LanguageSwitcher;
