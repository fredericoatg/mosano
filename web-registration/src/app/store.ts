import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import userRegistrationReducer from "../features/userRegistration/userRegistrationSlice";
import { useDispatch } from "react-redux";

export const store = configureStore({
  reducer: {
    userRegistration: userRegistrationReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
