// Small list of countries to serve the dropdown list
export const countries = [
    { name: "United States", code: "US" },
    { name: "Canada", code: "CA" },
    { name: "United Kingdom", code: "GB" },
    { name: "Australia", code: "AU" },
    { name: "Portugal", code: "PT" },
    { name: "Spain", code: "ES" },
    { name: "France", code: "FR" },
    { name: "Italy", code: "IT" },
    { name: "Germany", code: "DE" },
    { name: "Sweden", code: "SE" },
  ];
  