// calculateNextBirthday function in a separated file in order to be reusable if needed
export const calculateNextBirthday = (birthday: string) => {
  const birthDate = new Date(birthday);
  const today = new Date();

  let nextBirthday = new Date(
    today.getFullYear(),
    birthDate.getMonth(),
    birthDate.getDate()
  );

  if (nextBirthday < today) {
    nextBirthday.setFullYear(today.getFullYear() + 1);
  }

  const age = nextBirthday.getFullYear() - birthDate.getFullYear();

  return { nextBirthday, age };
};
