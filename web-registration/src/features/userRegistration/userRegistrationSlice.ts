import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import {
  fetchUserRegistrations,
  saveUserRegistration,
} from "../../api/userRegistrationAPI";
import { UserRegistrationFormData } from "./types";

const initialState = {
  registrations: [] as UserRegistrationFormData[],
  error: "",
};

export const userRegistrationSlice = createSlice({
  name: "userRegistration",
  initialState,
  reducers: {
    saveUserData: (state, action: PayloadAction<UserRegistrationFormData>) => {
      state.registrations = [...state.registrations, action.payload];
      state.error = "";
    },
    setRegistrations: (
      state,
      action: PayloadAction<UserRegistrationFormData[]>
    ) => {
      state.registrations = action.payload;
    },
    setError: (state, action: PayloadAction<string>) => {
      state.error = action.payload;
    },
  },
});

export const { saveUserData, setRegistrations, setError } = userRegistrationSlice.actions;

export const fetchRegistrations = () => async (dispatch: any) => {
  const registrations = await fetchUserRegistrations();
  dispatch(setRegistrations(registrations));
};

export const saveUserRegistrationData =
  (userData: UserRegistrationFormData) => async (dispatch: any) => {
    try {
      const savedData = await saveUserRegistration(userData);
      dispatch(saveUserData(savedData));
    } catch (error) {
      console.error("Error saving user registration:", error);
      dispatch(setError("Failed to save user registration."));
    }
  };

export const selectUserRegistrations = (state: RootState) =>
  state.userRegistration;

export default userRegistrationSlice.reducer;
