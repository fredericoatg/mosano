export interface UserRegistrationFormData {
  firstName: string;
  lastName: string;
  country: string;
  birthday: string;
}
