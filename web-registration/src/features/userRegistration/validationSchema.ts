import * as yup from "yup";

export const userRegistrationSchema = yup.object().shape({
  firstName: yup.string().required("First Name is required"),
  lastName: yup.string().required("Last Name is required"),
  country: yup.string().required("Country is required"),
  birthday: yup
    .string()
    .required("Birthday is required")
    .test(
      "is-date",
      "Birthday must be a valid date",
      (value) => !isNaN(Date.parse(value))
    )
    .test(
      "is-not-future",
      "Birthday cannot be in the future",
      (value) => new Date(value) <= new Date()
    ),
});
