import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { RootState, useAppDispatch } from "../../app/store";
import { saveUserRegistrationData, setError } from "./userRegistrationSlice";
import { userRegistrationSchema } from "./validationSchema";
import { UserRegistrationFormData } from "./types";
import { calculateNextBirthday } from "../../utils/dateUtils";
import "./User.css";
import { countries } from "../../utils/countries";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";

type CallbackFn = (input: string) => void;

const timeoutAction = (input: string, callback: CallbackFn, delay = 5000) => {
  const timer = setTimeout(() => {
    callback(input);
  }, delay);

  return () => clearTimeout(timer);
};

const UserRegistrationForm = () => {
  const { t } = useTranslation();
  const [message, setMessage] = useState<string>("");
  const dispatch = useAppDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<UserRegistrationFormData>({
    resolver: yupResolver(userRegistrationSchema),
  });
  const error = useSelector((state: RootState) => state.userRegistration.error);

  const onSubmit = async (data: UserRegistrationFormData) => {
    const { firstName, lastName, country, birthday } = data;
    const userData = {
      firstName,
      lastName,
      country,
      birthday,
    };
    try {
      await dispatch(saveUserRegistrationData(userData));
      reset();
    } catch (error) {
      // Do nothing here
    }

    const { nextBirthday, age } = calculateNextBirthday(data.birthday);
    setMessage(
      `Hello ${data.firstName} from ${
        data.country
      }. On ${nextBirthday.toLocaleDateString("en-US", {
        day: "numeric",
        month: "long",
      })} you will be ${age} years old!`
    );
  };

  useEffect(() => {
    if (message || error) {
      // Clean messages after some time
      const cleanup = timeoutAction(message || error, (input) => {
        if (input === message) {
          setMessage("");
        } else if (input === error) {
          dispatch(setError(""));
        }
      });
      return cleanup;
    }
  }, [message, error, dispatch]);

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="field-container">
          <label htmlFor="firstName">{t("form.firstName")}:</label>
          <div className="field-child-container">
            <input id="firstName" {...register("firstName")} />
            {errors.firstName && (
              <span className="error-text">{errors.firstName.message}</span>
            )}
          </div>
        </div>
        <div className="field-container">
          <label htmlFor="lastName">{t("form.lastName")}:</label>
          <div className="field-child-container">
            <input id="lastName" {...register("lastName")} />
            {errors.lastName && (
              <span className="error-text">{errors.lastName.message}</span>
            )}
          </div>
        </div>
        <div className="field-container">
          <label htmlFor="country">{t("form.country")}:</label>
          <div className="field-child-container">
            <select id="country" {...register("country")}>
              <option value="">{t("form.selectCountry")}</option>
              {countries.map((country, index) => (
                <option key={index} value={country.name}>
                  {country.name}
                </option>
              ))}
            </select>
            {errors.country && (
              <span className="error-text">{errors.country.message}</span>
            )}
          </div>
        </div>
        <div className="field-container">
          <label htmlFor="birthday">{t("form.birthday")}:</label>
          <div className="field-child-container">
            <input type="date" id="birthday" {...register("birthday")} />
            {errors.birthday && (
              <span className="error-text">{errors.birthday.message}</span>
            )}
          </div>
        </div>
        <div className="button-container">
          <button type="submit">{t("form.save")}</button>
        </div>
      </form>
      {!error && message && (
        <div className="succeed-message">
          <p>{message}</p>
        </div>
      )}
      {error && (
        <div className="error-message">
          <p>{error}</p>
        </div>
      )}
    </div>
  );
};

export default UserRegistrationForm;
