import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "../../app/store";
import "./User.css";
import { fetchRegistrations } from "./userRegistrationSlice";
import LoadingSpinner from "../../components/Spinner";
import { loadingTracker } from "../../api/api";
import { useTranslation } from "react-i18next";

const UserRegistrationInfo = () => {
  const { t } = useTranslation();
  const { registrations } = useSelector(
    (state: RootState) => state.userRegistration
  );
  const dispatch = useAppDispatch();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    dispatch(fetchRegistrations());

    const checkLoading = setInterval(() => {
      if (loadingTracker.isLoading) {
        setIsLoading(true);
      } else {
        setIsLoading(false);
      }
    }, 100);

    return () => {
      clearInterval(checkLoading);
    };
  }, [dispatch]);

  if (isLoading) return <LoadingSpinner />;

  return (
    <div className="info-container">
      <table className="table">
        <thead>
          <tr>
            <th>{t("form.name")}</th>
            <th>{t("form.country")}</th>
            <th>{t("form.birthday")}</th>
          </tr>
        </thead>
        <tbody>
          {registrations.map((user, index) => (
            <tr key={index}>
              <td>{user.firstName}</td>
              <td>{user.country}</td>
              <td>{user.birthday}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default UserRegistrationInfo;
