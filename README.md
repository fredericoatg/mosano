Country Data Management

## Description

This project consists of a small backend service using Node.js and Express, which fetches country data from a MongoDB database. It also includes a frontend application that allows users to list and add countries using a user-friendly interface. The 'services' folder includes the nodeJs backend and the 'web-registration' includes the frontend web application in ReactJs.

The backend service serves as an API endpoint for retrieving and storing country data, while the frontend application provides a convenient way for users to interact with the data.

## Features

- List countries: Users can view a list of countries stored in the database.
- Add country: Users can add new countries to the database using a form.
- API endpoints: The backend service exposes the following endpoints:
  - `GET /countries`: Retrieves a list of all countries.
  - `POST /countries`: Adds a new country to the database.

## Technologies used
    
    ### Services
    - Typescript
    - Express
    - MongoDB
    - mongoose
    ### web-registration
    - Typescript
    - ReactJs
    - Redux
    - Webpack
    - Axios
    - i18next




