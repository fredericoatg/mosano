import express from "express";
import mongoose from "mongoose";
import { countriesRouter } from "./controllers/countries";
import { authMiddleware } from "./middleware/auth";
import cors from "cors";

const app = express();
const port = process.env.PORT || 3001;

// Generally these auth keys should be served as environment variables
const mongodbKey = "mosano2023";
const uri = `mongodb+srv://fredericoatg:${mongodbKey}@cluster0.vkiaf2f.mongodb.net/node-c?retryWrites=true&w=majority`;

mongoose
  .connect(uri)
  .then(() => app.listen(port))
  .then(() => console.log(`Server is running on port ${port}`))
  .catch((err) => console.log(err));

app.use(express.json());

app.use(
  // cors({
  //   origin: "http://localhost:3000",
  //   methods: ["GET", "POST", "PUT", "DELETE"],
  // })
  cors()
);

app.use("/api/countries", authMiddleware, countriesRouter);
