import mongoose from "mongoose";

export interface Country extends mongoose.Document {
  id: string;
  firstName: string;
  lastName: string;
  country: string;
  birthday: string;
}

const CountrySchema = new mongoose.Schema({
  id: { type: String },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  country: { type: String, required: true },
  birthday: { type: String, required: true },
});

export const CountryModel = mongoose.model<Country>("Country", CountrySchema);
