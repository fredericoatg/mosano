import { Router } from "express";
import { CountryModel } from "../models/country";

export const countriesRouter = Router();

countriesRouter.get("/", async (req, res) => {
  try {
    const countries = await CountryModel.find();
    res.status(200).json(countries);
  } catch (err) {
    res.status(500).json({ error: "Error fetching countries" });
  }
});

countriesRouter.post("/", async (req, res) => {
  try {
    const newCountry = new CountryModel(req.body);
    await newCountry.save();
    res.status(201).json(newCountry);
  } catch (err) {
    res.status(500).json({ error: "Error creating country" });
  }
});

// Methods not needed for this usage
// countriesRouter.put("/:id", async (req, res) => {
//   try {
//     const country = await CountryModel.findByIdAndUpdate(
//       req.params.id,
//       req.body,
//       { new: true }
//     );
//     if (!country) {
//       return res.status(404).json({ error: "Country not found" });
//     }
//     res.status(200).json(country);
//   } catch (err) {
//     res.status(500).json({ error: "Error updating country" });
//   }
// });

// countriesRouter.delete("/:id", async (req, res) => {
//   try {
//     const country = await CountryModel.findByIdAndDelete(req.params.id);
//     if (!country) {
//       return res.status(404).json({ error: "Country not found" });
//     }
//     res.status(200).json(country);
//   } catch (err) {
//     res.status(500).json({ error: "Error deleting country" });
//   }
// });
