"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authMiddleware = void 0;
const authMiddleware = (req, res, next) => {
    try {
        const authHeader = req.headers.authorization;
        if (!authHeader) {
            return res.status(401).json({ error: "Authorization header missing" });
        }
        const token = authHeader.split(" ")[1];
        if (!token) {
            return res.status(401).json({ error: "Token missing" });
        }
        // Do some handling here to validate the token
        next();
    }
    catch (err) {
        res.status(403).json({ error: "Invalid token" });
    }
};
exports.authMiddleware = authMiddleware;
