"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CountryModel = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const CountrySchema = new mongoose_1.default.Schema({
    id: { type: String },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    country: { type: String, required: true },
    birthday: { type: String, required: true },
});
exports.CountryModel = mongoose_1.default.model("Country", CountrySchema);
