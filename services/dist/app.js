"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const countries_1 = require("./controllers/countries");
const auth_1 = require("./middleware/auth");
const cors_1 = __importDefault(require("cors"));
const app = (0, express_1.default)();
const port = process.env.PORT || 3001;
// Generally these auth keys should be served as environment variables
const mongodbKey = "mosano2023";
const uri = `mongodb+srv://fredericoatg:${mongodbKey}@cluster0.vkiaf2f.mongodb.net/node-c?retryWrites=true&w=majority`;
mongoose_1.default
    .connect(uri)
    .then(() => app.listen(port))
    .then(() => console.log(`Server is running on port ${port}`))
    .catch((err) => console.log(err));
app.use(express_1.default.json());
app.use(
// cors({
//   origin: "http://localhost:3000",
//   methods: ["GET", "POST", "PUT", "DELETE"],
// })
(0, cors_1.default)());
app.use("/api/countries", auth_1.authMiddleware, countries_1.countriesRouter);
