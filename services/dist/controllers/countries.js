"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.countriesRouter = void 0;
const express_1 = require("express");
const country_1 = require("../models/country");
exports.countriesRouter = (0, express_1.Router)();
exports.countriesRouter.get("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const countries = yield country_1.CountryModel.find();
        res.status(200).json(countries);
    }
    catch (err) {
        res.status(500).json({ error: "Error fetching countries" });
    }
}));
exports.countriesRouter.post("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const newCountry = new country_1.CountryModel(req.body);
        yield newCountry.save();
        res.status(201).json(newCountry);
    }
    catch (err) {
        res.status(500).json({ error: "Error creating country" });
    }
}));
// Methods not needed for this usage
// countriesRouter.put("/:id", async (req, res) => {
//   try {
//     const country = await CountryModel.findByIdAndUpdate(
//       req.params.id,
//       req.body,
//       { new: true }
//     );
//     if (!country) {
//       return res.status(404).json({ error: "Country not found" });
//     }
//     res.status(200).json(country);
//   } catch (err) {
//     res.status(500).json({ error: "Error updating country" });
//   }
// });
// countriesRouter.delete("/:id", async (req, res) => {
//   try {
//     const country = await CountryModel.findByIdAndDelete(req.params.id);
//     if (!country) {
//       return res.status(404).json({ error: "Country not found" });
//     }
//     res.status(200).json(country);
//   } catch (err) {
//     res.status(500).json({ error: "Error deleting country" });
//   }
// });
